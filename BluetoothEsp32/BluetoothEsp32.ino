// Copyright (c) Sandeep Mistry. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
/// New 14.07.2021 Old 14.07.2021
/// Oliver
/////////////
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>

#include <iostream>
#include <string>
#include <sstream>
/////////////
int ABCSW = 1;
int ERRORCODE = 0;
//////////////
BLEServer* pServer = NULL;
BLECharacteristic* pCharacteristic = NULL;
bool deviceConnected = false;
bool oldDeviceConnected = false;
int V = 49, SOC = 38, A = 23, ErrorNode = 0;
////////////
std::string toString(float i) {
  std::stringstream ss;
  ss << i;
  return ss.str();
}

#define SERVICE_UUID        "fcde0b6e-2de7-4554-a2f9-fc0338b7c8b3"
#define CHARACTERISTIC_UUID "6e22682d-fe86-4996-96e2-6cd287f24d09"

class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      deviceConnected = true;
    };

    void onDisconnect(BLEServer* pServer) {
      deviceConnected = false;
    }
};

//////////////////////////////////////////
//#include <CAN.h>
#include "ESP32SJA1000.h"
ESP32SJA1000Class CANESP;
#define ON  LOW
#define OFF HIGH
#include <EEPROM.h>

#include "SD.h"
#define SD_MOSI      18
#define SD_MISO      19
#define SD_SCK       5
#define SD_CS_PIN   14

File myFile;
SPIClass SPISD(HSPI);
String semi = ",";
bool checkonce = true;
int counterdatamissing = 0;
int LED_R = 2;
int LED_B = 4;
int LED_G = 15;

int CounterTime = 0;
//// Read data from frames
int Availablenode1 = 0;
int Availablenode2 = 0;
int ModulecommTimeout = 150; //(1 Min)
int ModulecommTimeoutRelay = 24; //(2 Min)
int timeoutVoltBatt1 = 0;
int timeoutVoltBatt2 = 0;
int timeoutTempBatt1 = 0;
int timeoutTempBatt2 = 0;
int timeoudelay = 12; // 12*5(60sec)

byte data181[8];
byte data581[8];
byte data582[8];
byte data182[8];
byte data281[8];
byte data282[8];

byte data305[8];
byte data306[8];

byte data1[8];
byte data2[8];
int dataIds[12];
int idscounter = 0, RelayStatus1Counter = 0, RelayStatus2Counter = 0;

byte Errbites0 = 0b00000000;
byte Errbites1 = 0b00000000;
byte Errbites2 = 0b00000000;
byte Errbites3 = 0b00000000;

double  Battery1Volt = 0, Battery2Volt = 0;

int counter = 0;
//// converted values after reading canframes
int batt1Vol = 0,   batt2Vol = 0;
int batt1Cur = 0,   batt2Cur = 0;
int batt1Soc = 0 ,  batt2Soc = 0;
int batt1Err = 0,   batt2Err = 0;
int batt1Temp = 0 , batt2Temp = 0, batt1Templow = 0, batt2Templow = 0;
int batt1SOH = 0 ,  batt2SOH = 0;
int Batter1NTC1 = 0, Batter1NTC2 = 0, Batter1NTC3 = 0;
int Batter2NTC1 = 0, Batter2NTC2 = 0, Batter2NTC3 = 0;
int Batt1RelaySta = 0, Batt2RelaySta = 0, SMAStatus = 0;
double Batt1CellMax = 0, Batt2CellMax = 0, Batt1CellMin = 0, Batt2CellMin = 0;

int Delaytimer = 0;

double  ScalingFVolt = 0.001;
double  ScalingFCurr = 0.03125;

int TempLimitHighArr = 50;
int TempLimitHighLeav = 45;

int TempLimitLowhArr = -10;
int TempLimitLowhLeav = -5;

int HighVolLimitArr = 59;
int HighVolLimitLeav = 57;

int underVolLimitArr = 42;  //42;
int underVolLimitLeav = 43; //43;

int DeltaVolt = 5;   ///5V 2 min timeout

int CounterWait = 0, CountSend = 0, CounterWaitDelta1 = 0, CounterWaitDelta2 = 0, SDOtimer = 0;

int Timerticks = 0, counterErrorRepeat = 0, longtimeresetRepErr = 0;
bool ActiveDeactiveTimer = false, ActiveDeactiveCounter = false, Errorcount1 = true;
int Battery1CurretErr = 0, Battery1VoltageErr = 0, Battery1UnderVoltErr = 0, Battery1TempErr = 0, Battery2CurretErr = 0, Battery2VoltageErr = 0, Battery2UnderVoltErr = 0, Battery2TempErr = 0;
void Sendreset1();
void Sendreset2();
int ErrorCounter = 5;
int SDayTime, SBatt1SOC, SBatt2SOC, SBatt1Volt, SBatt2Volt, SBatt1Curr, SBatt2Curr, SBatt1HotTemp, SBatt2HotTemp, SBatt1ColdTemp, SBatt2ColdTemp, SBatt1CellMax, SBatt2CellMax, SBatt1CellMin, SBatt2CellMin;
double SBatt1RelaySta, SBatt2RelaySta, SBatt1Error, SBatt2Error, SSMAVolt, SSMACurr, SSMAStatus, SSMATemp;

String Batt = "/Battery";
String EpromN = "0";
String Csv = ".csv";
String Filename ;

/////////////////////////////////////
void setup()
{
  Serial.begin(115200);
  while (!Serial);
  ///////////////////////////////////////////

  // Create the BLE Device
  BLEDevice::init("AentronBLE"); /// ID/Serial number

  // Create the BLE Server
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  // Create the BLE Service
  BLEService *pService = pServer->createService(SERVICE_UUID);

  // Create a BLE Characteristic
  pCharacteristic = pService->createCharacteristic(
                      CHARACTERISTIC_UUID,
                      BLECharacteristic::PROPERTY_WRITE  |
                      BLECharacteristic::PROPERTY_NOTIFY
                    );

  // Create a BLE Descriptor
  pCharacteristic->addDescriptor(new BLE2902());

  // Start the service
  pService->start();

  // Start advertising
  BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
  pAdvertising->addServiceUUID(SERVICE_UUID);
  pAdvertising->setScanResponse(true);
  pAdvertising->setMinPreferred(0x0);  // set value to 0x00 to not advertise this parameter
  BLEDevice::startAdvertising();
  Serial.println("Waiting a client connection to notify...");

  //////////////////////////////////////////
  pinMode(LED_R, OUTPUT);
  pinMode(LED_G, OUTPUT);
  pinMode(LED_B, OUTPUT);
  digitalWrite(LED_B, OFF);
  digitalWrite(LED_G, OFF);
  digitalWrite(LED_R, OFF);


  //////////////////////////////////////
  pinMode(SD_CS_PIN, OUTPUT);
  pinMode(SD_CS_PIN, INPUT_PULLUP);
  digitalWrite(SD_CS_PIN, HIGH);

  Serial.println("CAN Receiver Callback");
  // register the receive callback
  CANESP.onReceive(onReceive);
  CANESP.setPins(26, 25); /// CANbus
  CANESP.begin(500E3);    /// 500 kbits
  delay(2000);
}

void loop()
{
  CANESP.beginPacket(0x80);
  CANESP.write(0);
  CANESP.endPacket();
  delay(1000);
  ////////////////////////////////////////////
  batt1Soc  = data181[1];
  if (batt1Soc == 0) batt1Soc = 1;
  Serial.print("  BattSOC_1[%]:");
  Serial.print(batt1Soc);

  batt1Err =  data181[2];
  Serial.print("  BattErr_1:");

  if (batt1Err == 0)
  {
    Serial.print("No Error");
    Errbites1 = 0b10000000;
    Errbites0 = 0b10101010;
    Errbites2 = 0b00000000;
    Errbites3 = 0b00000010;
    Battery1CurretErr = 0;
    Battery1VoltageErr  = 0;
    Battery1UnderVoltErr = 0;
    Battery1TempErr = 0;
  }
  if (batt1Err > 0)
  {
    if (batt1Err == 0x02) {
      Serial.print("OVER CURRENT");
      Errbites1 = 0b01000000;
      if (Battery1CurretErr < ErrorCounter)
      {
        Sendreset1();
        Battery1CurretErr++;
      }
      ErrorNode = 1;
      ERRORCODE = 2;
    }
    if (batt1Err == 0x04) {
      Serial.print("OVER VOLTAGE");
      Errbites0 = 0b00000100;
      if (Battery1Volt < HighVolLimitLeav)
      {
        if (Battery1VoltageErr < ErrorCounter)
        {
          Sendreset1();
          Battery1VoltageErr++;
        }
      }
      ErrorNode = 1;
      ERRORCODE = 4;
    }
    if (batt1Err == 0x08) {
      Serial.print("UNDER VOLTAGE");
      Errbites0 = 0b00010000;
      if (Battery1Volt > underVolLimitLeav)
      {
        if (Battery1UnderVoltErr < ErrorCounter)
        {
          Sendreset1();
          Battery1UnderVoltErr++;
        }
      }
      ErrorNode = 1;
      ERRORCODE = 8;
    }
    if (batt1Err == 0x11) {
      Serial.print("DELTA VOLTAGE");
      Errbites3 = 0b00000001;
      ERRORCODE = 16;
      ErrorNode = 1;
    }
    if (batt1Err == 0x20) {
      Serial.print("TEMPERATURE");
      Errbites0 = 0b01000000;
      if ((batt1Temp < TempLimitHighLeav) && (batt1Temp > TempLimitLowhLeav))
      {
        if (Battery1TempErr < ErrorCounter)
        {
          Sendreset1();
          Battery1TempErr++;
        }
      }
      ErrorNode = 1;
      ERRORCODE = 32;
    }
    if (batt1Err == 0x80) {
      Serial.println("COMMUNICATION");
      Sendreset1();
      ERRORCODE = 128;
      ErrorNode = 1;
    }
    if (batt1Err == 0xFF) {
      Serial.println("EMV");
      Sendreset1();
      ErrorNode = 1;
      ERRORCODE = 255;
    }

  }
  ///////////////////////////////////////////////////
  batt1Vol = (data181[7] << 8) | data181[6];
  Serial.print("  BattVol_1[V]:");
  Battery1Volt = batt1Vol * ScalingFVolt;
  Serial.println(batt1Vol * ScalingFVolt);

  //////////////////////////////////////////////////
  batt2Soc  = data182[1];
  if (batt2Soc == 0) batt2Soc = 1;
  Serial.print("  BattSOC_2[%]:");
  Serial.print(batt2Soc);

  batt2Err =  data182[2];
  Serial.print("  BattErr_2:");
  if (batt2Err == 0)
  {
    if (batt1Err == 0)
    {
      Serial.print("No Error");
      Errbites1 = 0b10000000;
      Errbites0 = 0b10101010;
      Errbites2 = 0b00000000;
      Errbites3 = 0b00000010;
      Battery2CurretErr = 0;
      Battery2VoltageErr = 0;
      Battery2UnderVoltErr = 0;
      Battery2TempErr = 0;
      ERRORCODE = 0;
      ErrorNode = 0;
    }
  }
  if (batt2Err == 0x02) {
    Serial.println("OVER CURRENT");
    Errbites1 = 0b01000000;
    if (Battery2CurretErr < ErrorCounter)
    {
      Sendreset2();
      Battery2CurretErr++;
      Serial.print("Battery1CurretErr: ");
      Serial.println(Battery2CurretErr);
    }
    ErrorNode = 2;
    ERRORCODE = 2;
  }
  if (batt2Err == 0x04) {
    Serial.print("OVER VOLTAGE");
    Errbites0 = 0b00000100;
    if (Battery2Volt < HighVolLimitLeav)
    {
      if (Battery2VoltageErr < ErrorCounter)
      {
        Sendreset2();
        Battery2VoltageErr++;
      }
    }
    ErrorNode = 2;
    ERRORCODE = 4;
  }
  if (batt2Err == 0x08) {
    Serial.print("UNDER VOLTAGE");
    Errbites0 = 0b00010000;
    if (Battery2Volt > underVolLimitLeav)
    {
      if (Battery2UnderVoltErr < ErrorCounter)
      {
        Sendreset2();
        Battery2UnderVoltErr++;
      }
    }
    ErrorNode = 2;
    ERRORCODE = 8;
  }
  if (batt2Err == 0x11) {
    Serial.println("DELTA VOLTAGE");
    Errbites3 = 0b00000001;
    ErrorNode = 2;
    ERRORCODE = 16;
  }
  if (batt2Err == 0x20) {
    Serial.print("TEMPERATURE");
    Errbites0 = 0b01000000;
    if ((batt2Temp < TempLimitHighLeav) && (batt2Temp > TempLimitLowhLeav))
    {
      if (Battery2TempErr < ErrorCounter)
      {
        Sendreset2();
        Battery2TempErr++;
      }
    }
    ErrorNode = 2;
    ERRORCODE = 32;
  }
  if (batt2Err == 0x80) {
    Serial.print("COMMUNICATION");
    Sendreset2();
    ErrorNode = 2;
    ERRORCODE = 128;
  }
  if (batt2Err == 0xFF) {
    Serial.print("UNKNWON");
    Sendreset2();
    ErrorNode = 2;
    ERRORCODE = 255;
  }
  delay(1000); /// 1000 Not good
  if ((batt2Err > 0) || (batt1Err > 0))
  {
    digitalWrite(LED_G, OFF);
    digitalWrite(LED_G, OFF);

    digitalWrite(LED_R, ON);
    digitalWrite(LED_R, ON);
    delay(100);
  }

  /////////////////////////////////////////////////////
  batt2Vol = (data182[7] << 8) | data182[6];
  Serial.print("  BattVol_2[V]:");
  Battery2Volt = batt2Vol * ScalingFVolt;
  Serial.println(Battery2Volt);

  if ((Battery2Volt > HighVolLimitArr) || (Battery1Volt > HighVolLimitArr))
  {
    timeoutVoltBatt2++;
    if (timeoutVoltBatt2 > timeoudelay)
    {
      Errbites0 = 0b00000100;
    }
  }
  else
  {
    timeoutVoltBatt2 = 0;
  }
  if ((Battery2Volt < underVolLimitArr ) || (Battery1Volt < underVolLimitArr))
  {
    timeoutVoltBatt1++;
    if (timeoutVoltBatt1 > timeoudelay)
    {
      Errbites0 = 0b00010000;
    }
  }
  else
  {
    timeoutVoltBatt1 = 0;
  }

  /////////////////////////
  /// Original Data
  String MSB = String(data281[7], HEX);
  String LSB = String(data281[6], HEX);
  ////////////////////
  //////////////////////////////////////////////////
  String MSB1 = String(data181[5], HEX);
  String LSB1 = String(data181[4], HEX);
  signed int batt1Cur = (int16_t)(strtol((MSB1 + LSB1).c_str(), NULL, 16));
  double  Batt1Current = batt1Cur * ScalingFCurr;
  Serial.print("  BattCurr_1[A]:");
  Serial.print(batt1Cur * ScalingFCurr);
  //  Serial.print(" LSB1:");
  //  Serial.print(data181[4], HEX);
  //  Serial.print(" MSB1:");
  //  Serial.print(data181[5], HEX);
  /////////////////////////////////////////////
  String MSB2 = String(data182[5], HEX);
  String LSB2 = String(data182[4], HEX);
  signed int batt2Cur = (int16_t)(strtol((MSB2 + LSB2).c_str(), NULL, 16));
  double  Batt2Current = batt2Cur * ScalingFCurr;
  Serial.print("  BattCurr_2[A]:");
  Serial.print(batt2Cur * ScalingFCurr);
  //  Serial.print(" LSB1:");
  //  Serial.print(data182[4], HEX);
  //  Serial.print(" MSB1:");
  //  Serial.println(data182[5], HEX);
  ///////////////////////////////////////////////
  Serial.print("  BattTemp_1[°C]:");
  batt1Temp = data181[3] ;
  Serial.print(batt1Temp);

  /////////////////////////////////////////////
  batt2Temp = data182[3] ;
  Serial.print("  BattTemp_2[°C]:");
  Serial.println(batt2Temp);

  if ((batt2Temp > TempLimitHighArr) || (batt1Temp > TempLimitHighArr))
  {
    timeoutTempBatt2++;
    if (timeoutTempBatt2 > timeoudelay)
    {
      Errbites0 = 0b01000000;
    }
  }
  else
  {
    timeoutTempBatt2 = 0;
  }
  if ((batt2Temp < TempLimitLowhArr ) || (batt1Temp < TempLimitLowhArr))
  {
    timeoutTempBatt1++;
    if (timeoutTempBatt1 > timeoudelay)
    {
      Errbites0 = 0b01000000;
    }
  }
  else
  {
    timeoutTempBatt1 = 0;
  }

  ////////////////////////////////////////////
  batt1SOH = data181[0] ;
  //  Serial.print("  Battery1SOH :");
  //  Serial.print(batt1SOH);
  /////////////////////////////////////////////
  batt2SOH = data182[0] ;
  //  Serial.print("  Battery2SOH");
  //  Serial.println(batt2SOH);
  ////////////////////////////////////////////
  ////////////////////////////////////////////
  /// SMA Data Reading Every 3Sec
  ////////////////////////////////////////////
  int battVol305 = (data305[1] << 8) | data305[0];
  Serial.print("  battVol305:");
  Serial.print(battVol305 * 0.1);
  SSMAVolt = battVol305 * 0.1;

  String MSB3 = String(data305[2], HEX);
  String LSB3 = String(data305[3], HEX);
  signed int battCurr305 = (int16_t)(strtol((MSB3 + LSB3).c_str(), NULL, 16));
  Serial.print("  battCurr305:");
  SSMACurr  = battCurr305 * 0.1;
  Serial.print(battCurr305 * 0.1);

  int battTemp305 = ((data305[5] << 8) | data305[4]);
  Serial.print("  battTemp305:");
  Serial.print(battTemp305 * 0.1);
  SSMATemp = battTemp305 * 0.1;

  int battSOC305 = (data305[7] << 8) | data305[6];
  Serial.print("  battSOC305:");
  Serial.println(battSOC305 * 0.1);

  ///////////////////////////////////////////
  int battSOH306 = (data306[1] << 8) | data306[0];
  Serial.print("  battSOH306:");
  Serial.print(battSOH306);

  int battChPr306 = data306[2] ;
  Serial.print("  battChProce306:");
  Serial.print(battChPr306);

  int battOpeSta306 = data306[3];
  Serial.print("  battOpeSta306:");
  Serial.print(battOpeSta306);

  int battErrorM306 = (data306[5] << 8) | data306[4];
  Serial.print("  battErrorM306:");
  Serial.print(battErrorM306);
  SMAStatus = battErrorM306;

  int battBattChaVol306 = (data306[7] << 8) | data306[6];
  Serial.print("  battBattChaVol306:");
  Serial.println(battBattChaVol306 * 0.1);

  //////////////////////////////////////////////
  //// When the Batteries have more than 20V and also differnece is greater than 8V Difference
  /// when we remove the 0x181 battery then we see same as old values , but we will repleaser them with 0 values after Read or at then end of Loop better or after 3 counter then Reset to 0
  if ((batt1Err > 0) && (batt2Err == 0))
  {
    Serial.println(CounterWaitDelta1);
    CounterWaitDelta1++;
    if ((CounterWaitDelta1 > 10) && (CounterWaitDelta1 < 20))
    {
      CANESP.beginPacket(0x182);
      CANESP.write(0);
      CANESP.write(0);
      CANESP.write(10);
      CANESP.write(0); /// Charge Current 1420 -> 0.1
      CANESP.write(0);
      CANESP.write(0); /// Disch Current  3000 -> 0.1
      CANESP.write(0);
      CANESP.write(0); /// Disch Voltage  430 -> 0.1
      CANESP.endPacket();
      delay(500);
    }
  }
  else
  {
    CounterWaitDelta1 = 0;
  }

  if ((batt2Err > 0) && (batt1Err == 0))
  {
    Serial.println(CounterWaitDelta2);
    CounterWaitDelta2++;
    if ((CounterWaitDelta2 > 10) && (CounterWaitDelta2 < 20))
    {
      CANESP.beginPacket(0x181);
      CANESP.write(0);
      CANESP.write(0);
      CANESP.write(10);
      CANESP.write(0);
      CANESP.write(0);
      CANESP.write(0);
      CANESP.write(0);
      CANESP.write(0);
      CANESP.endPacket();
      delay(500);
    }
  }
  else
  {
    CounterWaitDelta2 = 0;
  }

  //////////////////////////////////////////////
  //////////////////////////////////////////////
  /// Average Algorithem
  int SocDivi = 1, BLESoc;
  if ((batt1Soc > 2) && (batt2Soc > 2)) SocDivi = 2;
  int SMASoc = (batt1Soc + batt2Soc) / SocDivi;
  BLESoc = SMASoc;
  Serial.print("  SMASoc:");
  Serial.print(SMASoc);
  ///////////////////////////////////////////
  int SoHDivi = 1 , BLESoH;
  if ((batt1SOH > 2) && (batt2SOH > 2)) SoHDivi = 2;
  int SMASOH = (batt1SOH + batt2SOH) / SoHDivi;
  BLESoH = SMASOH;
  Serial.print("  SMASOH:");
  Serial.print(SMASOH);
  ///////////////////////////////////////////
  int VoltDivi = 1;
  float BLEVolt;
  if ((Battery2Volt > 20) && (Battery1Volt > 20)) VoltDivi = 2;
  int SMABattVoltage = ((Battery1Volt + Battery2Volt) / VoltDivi) * 100;
  BLEVolt = ((Battery1Volt + Battery2Volt) / VoltDivi);
  int TempBLEVolt = BLEVolt * 10;
  BLEVolt = (float)TempBLEVolt / 10.0;
  Serial.print("  SMABattVoltage:");
  Serial.print(SMABattVoltage);

  int CurrentDivi = 2;
  float BLECurr;
  //if ((Battery1Volt > 20) && (Battery1Volt > 20)) VoltDivi = 2;
  int SMABattcurrent = ((Batt1Current + Batt2Current)) * 10;
  BLECurr = ((Batt1Current + Batt2Current));
  int TempBLECurr = BLECurr * 10;
  BLECurr = (float)TempBLECurr / 10;
  Serial.print("  SMABattcurrent:");
  Serial.print(SMABattcurrent);

  int TempDivi = 2;
  // if ((Battery1Volt > 20) && (Battery1Volt > 20)) VoltDivi = 2;
  int SMABattTemp = ((batt1Temp + batt2Temp) / VoltDivi) * 10;
  int BLETemp = ((batt1Temp + batt2Temp) / VoltDivi);
  Serial.print("  ErrorNode:");
  Serial.println(ErrorNode);

  for (int i = 0; i < 12; i++)
  {
    if ((dataIds[i] == 385) || (dataIds[i] == 641) || (dataIds[i] == 897) || (dataIds[i] == 1153))
    {
      Availablenode1 = 0;
    }
    else
    {
      Availablenode1++;
    }
    if ((dataIds[i] == 386) || (dataIds[i] == 642) || (dataIds[i] == 898) || (dataIds[i] == 1154))
    {
      Availablenode2 = 0;
    }
    else
    {
      Availablenode2++;
    }
    dataIds[i] = 0;
  }

  if (Availablenode1 > ModulecommTimeout)  Errbites0 = 0b00000001;
  if (Availablenode2 > ModulecommTimeout)  Errbites0 = 0b00000001;
  //////////////////////////////////////////
  /////////////////////////////////////////
  ////////////////////////////////
  /// Relay Status and Software Version
  ///////////////////////////////
  SDOtimer++;
  if (SDOtimer > 7234) SDOtimer = 0;

  if ((SDOtimer == 1) || (SDOtimer > 7200)) /// 7200 2 Hours
  {
    CANESP.beginPacket(0x601);
    CANESP.write(0x40);
    CANESP.write(0x00);
    CANESP.write(0x61);
    CANESP.write(0x01);
    CANESP.write(0x00);
    CANESP.write(0x00);
    CANESP.write(0x00);
    CANESP.write(0x00);
    CANESP.endPacket();
    ///////////////////////////////
    CANESP.beginPacket(0x602);
    CANESP.write(0x40);
    CANESP.write(0x00);
    CANESP.write(0x61);
    CANESP.write(0x01);
    CANESP.write(0x00);
    CANESP.write(0x00);
    CANESP.write(0x00);
    CANESP.write(0x00);
    CANESP.endPacket();
  }

  //  if (RelayStatus1Counter > ModulecommTimeoutRelay)  Errbites0 = 0b00000001;
  //  if (RelayStatus2Counter > ModulecommTimeoutRelay)  Errbites0 = 0b00000001;
  /////////////////////////////////////////////
  Batt1CellMax = ((data281[0] + data282[0]) / 2)  * 0.02;
  Batt2CellMin = ((data281[1] + data282[1]) / 2)   * 0.02 ;

  /* Batt1CellMin  = data281[1] * 0.02;
    Batt2CellMin  = data282[1] * 0.02;*/

  float Capacity_Left = ((float)batt1Soc * 40.0) / 100.0;   ///BLESoc
  int Remain_Time = ((40.0 - (float)Capacity_Left) * abs(batt1Cur)) / 60 ;

  /////////////////////////////////////////////
  Batt1RelaySta = data281[2];
  Batt2RelaySta = data282[2];
  int BLEStatus = 0;
  if ((Batt1RelaySta > 0) && (Batt2RelaySta > 0)) BLEStatus = 1;
  /////////////////////////////////////////////
  /// Bluetooth Data sending
  // notify changed value
  if (deviceConnected) {
    std::string s = "&SOC=" + toString(BLESoc) +  "&MOTOR=" + toString(200) + "&V=" + toString(BLEVolt) +  "&A=" + toString(BLECurr) +  "&TEMP=" + toString(BLETemp) + "&STATUS=" + toString(BLEStatus)
                    + "&REMAINTIME=" + toString(Remain_Time) + "&MAXCELL=" + toString(Batt1CellMax) + "&MINCELL=" + toString(Batt2CellMin) + "&SOH=" + toString(BLESoH) + "&RUNTIME=" + toString(60)
                    + "&LIFECHARGED=" + toString(60) + "&ERRORCODE=" + toString(ERRORCODE) + "&LASTERROR=" + toString(0) + "&SWABC=" + toString(0) + toString(0) + toString(ABCSW)
                    + "&SWNAME=" "AMMS" +  "&SWVALUE=" +  toString(0) + toString(52) + "&ERRORNODE=" +  toString(1) + "&ERRORLIST=" +  toString(2);  /// battery-half-outline

    pCharacteristic->setValue(s);
    pCharacteristic->notify();
  }
  // disconnecting
  if (!deviceConnected && oldDeviceConnected) {
    delay(500); // give the bluetooth stack the chance to get things ready
    pServer->startAdvertising(); // restart advertising
    Serial.println("start advertising");
    oldDeviceConnected = deviceConnected;
  }
  // connecting
  if (deviceConnected && !oldDeviceConnected) {
    // do stuff here on connecting
    oldDeviceConnected = deviceConnected;
  }
  ////////////////////////////////////////////
}

void onReceive(int packet)
{
  Serial.print("packet with id 0x");
  Serial.println(CANESP.packetId(), HEX);

  dataIds[idscounter] = CANESP.packetId();
  idscounter++;
  if (idscounter > 12) idscounter = 0;

  for (int i = 0; i < 8; i++)
  {
    if (CANESP.available())
    {
      counterdatamissing = 0;
      if (CANESP.packetId() == 0x181)
      {
        data181[i] = CANESP.read();
        digitalWrite(LED_G, ON);
        digitalWrite(LED_R, OFF);
      }
      else if (CANESP.packetId() == 0x281)
      {
        data281[i] = CANESP.read();
        digitalWrite(LED_G, ON);
        digitalWrite(LED_R, OFF);
      }
      else if (CANESP.packetId() == 0x282)
      {
        data282[i] = CANESP.read();
        digitalWrite(LED_G, ON);
        digitalWrite(LED_R, OFF);
      }

      else if (CANESP.packetId() == 0x182)
      {
        data182[i] = CANESP.read();
        digitalWrite(LED_G, ON);
        digitalWrite(LED_R, OFF);
      }
    }
    else
    {
      // Serial.println("data Received times Max 20");
      // Serial.print(counterdatamissing);
      counterdatamissing++;
      if (counterdatamissing > 20)
      {
        Serial.println("No data Received");
        digitalWrite(LED_R, ON);
        digitalWrite(LED_G, OFF);
      }
    }
  }
}
void Sendreset1()
{
  for (int i = 0; i < 5; i++)
  {
    CANESP.beginPacket(0x81);
    CANESP.write(3);
    CANESP.endPacket();
    Serial.println("Reset");
    delay(300);
  }
}
void Sendreset2()
{
  for (int i = 0; i < 5; i++)
  {
    CANESP.beginPacket(0x81);
    CANESP.write(4);
    CANESP.endPacket();
    Serial.println("Reset");
    delay(300);
  }
}

// ID=305,Type=D,Length=8,Data=EF 01 FF FA DC 00 E8 03,CycleTime=5000,Paused=1,IDFormat=hex
