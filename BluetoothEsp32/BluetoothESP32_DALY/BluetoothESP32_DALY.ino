// Copyright (c) Sandeep Mistry. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// New 14.07.2021 Old 14.07.2021
// Oliver
/////////////
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>

#include <iostream>
#include <string>
#include <sstream>
/////////////
int ABCSW = 1;
int ERRORCODE = 0;
//////////////
BLEServer* pServer = NULL;
BLECharacteristic* pCharacteristic = NULL;
bool deviceConnected = false;
bool oldDeviceConnected = false;
int V = 49, SOC = 38, A = 23, ErrorNode = 0;
float BLESoc = 0, BLEVolt = 0, BLECurr = 0, BattCellMax = 0, BattCellMin = 0;
int BLEStatus = 0, Remain_Time = 0, BLESoH = 0, BLERuntime = 0, BLELifecharg = 0, BLEErrorcode = 0, BLELasterror = 0, BLE_ABCSW = 0 , BLE_SWVALUE_Daly = 0 ,
    BLE_Errornode = 0, BLETemp = 0;
int ERRORLISTint = 0, ERRORNODEint = 0, ERRORCODEint = 0;
int Serialprio1 = 0;
int Serialprio2 = 0;

////////////
std::string toString(float i) {
  std::stringstream ss;
  ss << i;
  return ss.str();
}

#define SERVICE_UUID        "fcde0b6e-2de7-4554-a2f9-fc0338b7c8b3"
#define CHARACTERISTIC_UUID "6e22682d-fe86-4996-96e2-6cd287f24d09"

class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      deviceConnected = true;
    };

    void onDisconnect(BLEServer* pServer) {
      deviceConnected = false;
    }
};

//////////////////////////////////////////
//#include <CAN.h>
#include "ESP32SJA1000.h"
ESP32SJA1000Class CANESP;
#define ON  LOW
#define OFF HIGH
#include <EEPROM.h>

#include "SD.h"
#define SD_MOSI      18
#define SD_MISO      19
#define SD_SCK       5
#define SD_CS_PIN   14

File myFile;
SPIClass SPISD(HSPI);
String semi = ",";
bool checkonce = true;
int counterdatamissing = 0;
int LED_R = 2;
int LED_B = 4;
int LED_G = 15;

int CounterTime = 0;
//// Read data from frames
int Availablenode1 = 0;
int Availablenode2 = 0;
int ModulecommTimeout = 150; //(1 Min)
int ModulecommTimeoutRelay = 24; //(2 Min)
int timeoutVoltBatt1 = 0;
int timeoutVoltBatt2 = 0;
int timeoutTempBatt1 = 0;
int timeoutTempBatt2 = 0;
int timeoudelay = 12; // 12*5(60sec)

byte data90_1[8];
byte data91_1[8];
byte data92_1[8];
byte data93_1[8];
byte data94_1[8];
byte data98_1[8];

byte data90_2[8];
byte data91_2[8];
byte data92_2[8];
byte data93_2[8];
byte data94_2[8];
byte data98_2[8] = {0, 0, 0, 0, 0, 0, 0, 0};

byte data305[8];
byte data306[8];
byte data1[8];
byte data2[8];
int dataIds[12];
int dataMotorrmp[8];

byte datacan[3][8];
int idscounter = 0, RelayStatus1Counter = 0, RelayStatus2Counter = 0;

byte Errbites0 = 0b00000000;
byte Errbites1 = 0b00000000;
byte Errbites2 = 0b00000000;
byte Errbites3 = 0b00000000;

double  Battery1Volt = 0, Battery2Volt = 0;

int counter = 0;
//// converted values after reading canframes
int batt1Vol = 0,   batt2Vol = 0;
int batt1Cur = 0,   batt2Cur = 0;
int batt1Soc = 0 ,  batt2Soc = 0;
int batt1Err = 0,   batt2Err = 0;
int batt1Temp = 0 , batt2Temp = 0, batt1Templow = 0, batt2Templow = 0;
int batt1SOH = 0 ,  batt2SOH = 0;
int Batter1NTC1 = 0, Batter1NTC2 = 0, Batter1NTC3 = 0;
int Batter2NTC1 = 0, Batter2NTC2 = 0, Batter2NTC3 = 0;
int Batt1RelaySta = 0, Batt2RelaySta = 0, SMAStatus = 0;
double Batt1CellMax = 0, Batt2CellMax = 0, Batt1CellMin = 0, Batt2CellMin = 0;

int Delaytimer = 0;

double  ScalingFVolt = 0.001;
double  ScalingFCurr = 0.03125;

int TempLimitHighArr = 50;
int TempLimitHighLeav = 45;

int TempLimitLowhArr = -10;
int TempLimitLowhLeav = -5;

int HighVolLimitArr = 59;
int HighVolLimitLeav = 57;

int underVolLimitArr = 42;  //42;
int underVolLimitLeav = 43; //43;

int DeltaVolt = 5;   ///5V 2 min timeout

int CounterWait = 0, CountSend = 0, CounterWaitDelta1 = 0, CounterWaitDelta2 = 0, SDOtimer = 0;

int Timerticks = 0, counterErrorRepeat = 0, longtimeresetRepErr = 0;
bool ActiveDeactiveTimer = false, ActiveDeactiveCounter = false, Errorcount1 = true;
int Battery1CurretErr = 0, Battery1VoltageErr = 0, Battery1UnderVoltErr = 0, Battery1TempErr = 0, Battery2CurretErr = 0, Battery2VoltageErr = 0, Battery2UnderVoltErr = 0, Battery2TempErr = 0;
void Sendreset1();
void Sendreset2();
int ErrorCounter = 5;
int SDayTime, SBatt1SOC, SBatt2SOC, SBatt1Volt, SBatt2Volt, SBatt1Curr, SBatt2Curr, SBatt1HotTemp, SBatt2HotTemp, SBatt1ColdTemp, SBatt2ColdTemp, SBatt1CellMax, SBatt2CellMax, SBatt1CellMin, SBatt2CellMin;
double SBatt1RelaySta, SBatt2RelaySta, SBatt1Error, SBatt2Error, SSMAVolt, SSMACurr, SSMAStatus, SSMATemp;

int Binarydata[8] = {0, 0, 0, 0, 0, 0, 0, 0};
int Binarydata1[8] = {0, 0, 0, 0, 0, 0, 0, 0};
String Batt = "/Battery";
String EpromN = "0";
String Csv = ".csv";
String Filename;
std::string errorName, errorIcon, swName, BLE_ErrorList;
//String BLE_ErrorList = "0";


/////////////////////////////////////
void setup()
{
  Serial.begin(115200);
  while (!Serial);
  ///////////////////////////////////////////
  // Create the BLE Device
  BLEDevice::init("AentronBLE-001"); /// ID/Serial number

  // Create the BLE Server
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  // Create the BLE Service
  BLEService *pService = pServer->createService(SERVICE_UUID);

  // Create a BLE Characteristic
  pCharacteristic = pService->createCharacteristic(
                      CHARACTERISTIC_UUID,
                      BLECharacteristic::PROPERTY_WRITE  |
                      BLECharacteristic::PROPERTY_NOTIFY
                    );

  // Create a BLE Descriptor
  pCharacteristic->addDescriptor(new BLE2902());

  // Start the service
  pService->start();

  // Start advertising
  BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
  pAdvertising->addServiceUUID(SERVICE_UUID);
  pAdvertising->setScanResponse(true);
  pAdvertising->setMinPreferred(0x0);  // set value to 0x00 to not advertise this parameter
  BLEDevice::startAdvertising();
  Serial.println("Waiting a client connection to notify...");

  //////////////////////////////////////////
  pinMode(LED_R, OUTPUT);
  pinMode(LED_G, OUTPUT);
  pinMode(LED_B, OUTPUT);
  digitalWrite(LED_B, OFF);
  digitalWrite(LED_G, OFF);
  digitalWrite(LED_R, OFF);

  //////////////////////////////////////
  pinMode(SD_CS_PIN, OUTPUT);
  pinMode(SD_CS_PIN, INPUT_PULLUP);
  digitalWrite(SD_CS_PIN, HIGH);

  Serial.println("CAN Receiver Callback");
  // register the receive callback
  CANESP.onReceive(onReceive);
  CANESP.setPins(26, 25); /// CANbus
  CANESP.begin(250E3);    /// 500 kbits
  delay(2000);
}

void loop()
{
  int Data90 = 0x00900000; int Data91 = 0x00910000;  int Data92 = 0x00920000; int Data93 = 0x00930000;
  int Data94 = 0x00940000; int Data98 = 0x00980000;

  int BMSID = 0x00000100;
  DalyCanbus1(Data90, BMSID);
  DalyCanbus1(Data91, BMSID);
  DalyCanbus1(Data92, BMSID);
  DalyCanbus1(Data93, BMSID);
  DalyCanbus1(Data94, BMSID);
  DalyCanbus1(Data98, BMSID);

  BMSID = 0x00000200;
  DalyCanbus1(Data90, BMSID);
  DalyCanbus1(Data91, BMSID);
  DalyCanbus1(Data92, BMSID);
  DalyCanbus1(Data93, BMSID);
  DalyCanbus1(Data94, BMSID);
  DalyCanbus1(Data98, BMSID);

  float divider = 2.0;
  float SoC1 = (data90_1[6] << 8  | data90_1[7]) * 0.1;
  float SoC2 = (data90_2[6] << 8 | data90_2[7]) * 0.1;
  BLESoc = (SoC1 + SoC2) / divider;
  if (Serialprio1)
  {
    Serial.print(" SoC: ");
    Serial.print(BLESoc);
  }

  float volt1 = (data90_1[0] << 8 | data90_1[1]) * 0.1;
  float volt2 = (data90_2[0] << 8 | data90_2[1]) * 0.1;
  BLEVolt = (volt1 + volt2) / divider;
  if (Serialprio1)
  {
    Serial.print(" BLEVolt: ");
    Serial.print(BLEVolt);
  }

  float curr1 = (data90_1[4] << 8 | data90_1[5]) - 30000.0;
  float curr2 = (data90_2[4] << 8 | data90_2[5]) - 30000.0;
  BLECurr = (curr1 + curr2);
  if (Serialprio1)
  {
    Serial.print(" BLECurr: ");
    Serial.print(BLECurr);
  }

  float temp1 = (data92_1[0]) - 40.0;
  float temp2 = (data92_2[0]) - 40.0;
  BLETemp = (temp1 + temp2) / divider;
  if (Serialprio1)
  {
    Serial.print(" BLETemp: ");
    Serial.print(BLETemp);
  }

  int MOSCharStatus1 = (data93_1[1]);
  int MOSCharStatus2 = (data93_2[1]);
  int MOSchar = (MOSCharStatus1 + MOSCharStatus2) / 2;

  int MOSdischarStatus1 = (data93_1[2]);
  int MOSdischarStatus2 = (data93_2[2]);
  int MOSdischar = (MOSdischarStatus1 + MOSdischarStatus2) / 2;
  BLEStatus = (MOSchar + MOSdischar) / divider;
  if (Serialprio1)
  {
    Serial.print(" BLEStatus: ");
    Serial.print(BLEStatus);
  }

  int BattCapa1 = (data93_1[6] << 8 | data93_1[7]);
  int BattCapa2 = (data93_2[6] << 8 | data93_2[7]);
  float BattaH = (BattCapa1 + BattCapa2) / 2000.0;
  if (Serialprio1)
  {
    Serial.print(" BattaH: ");
    Serial.println(BattaH);
  }

  Remain_Time = BattaH / BLECurr;

  float Batt1CellMax = (data91_1[0] << 8 | data91_1[1]);
  float Batt2CellMax = (data91_2[0] << 8 | data91_2[1]);
  BattCellMax = ((Batt1CellMax + Batt2CellMax) / 2000.0);
  int Temp1 = BattCellMax * 100.0;
  BattCellMax = (float)Temp1 / 100.0;
  if (Serialprio1)
  {
    Serial.print(" BattCellMax: ");
    Serial.print(BattCellMax);
  }

  float Batt1CellMin = (data91_1[3] << 8 | data91_1[4]);
  float Batt2CellMin = (data91_2[3] << 8 | data91_2[4]);
  BattCellMin = ((Batt1CellMin + Batt2CellMin) / 2000.0);
  int Temp2 = BattCellMin * 100.0;
  BattCellMin = (float)Temp2 / 100.0;
  if (Serialprio1)
  {
    Serial.print(" BattCellMin: ");
    Serial.print(BattCellMin);
  }

  int SoH1 = (data93_1[3]);
  int SoH2 = (data93_2[3]);
  BLESoH = ((SoH1 + SoH2) / divider);
  if (Serialprio1)
  {
    Serial.print(" BLESoH: ");
    Serial.println(BLESoH);
  }

  BLERuntime = 0; /// ?
  BLELifecharg = 0; /// ?

  if (Serialprio1)
  {
    Serial.print(" ERRORNODEint: ");
    Serial.print(ERRORNODEint);
    Serial.print(" BLEErrorcode: ");
    Serial.println(BLEErrorcode);
  }

  BLELasterror = 0;
  BLE_ABCSW = 0;
  BLE_SWVALUE_Daly = 0;

  for (int i = 0; i < 8; i++)
  {
    if (Serialprio2) Serial.println(data98_1[i]);
    if (data98_1[i] > 0) convertDecimalToBinary(i, data98_1[i], 1);

    if (Serialprio2) Serial.println(data98_2[i]);
    if (data98_2[i] > 0) convertDecimalToBinary(i, data98_2[i], 2);
  }
  // 0 = low voltage, 1 = high voltage, 2 = too cold, 3 = too hot
  // string errorName = "Low Voltage,Too Cold,Some other error";

  ERRORLISTint = ERRORLISTint  + 1;
  if (ERRORLISTint > 5) ERRORLISTint = 1;

  ERRORCODEint = ERRORCODEint  + 1;
  if (ERRORCODEint >= 130) ERRORCODEint = 0;
  /*
     battery-half-outline
     snow-outline
     battery-full-outline
     thermometer-outline
  */

  //// Motor Rpm
  int motorRpm = (dataMotorrmp[3] << 8 | dataMotorrmp[2]);
  if (Serialprio1)
  {
    Serial.print(" motorRpm: ");
    Serial.print(motorRpm);
  }

  /////////////////////////////////////////////
  // Bluetooth Data sending
  // notify changed value
  if (deviceConnected) {
    std::string s =   "&a=" + toString(BLESoc) + "&b=" + toString(motorRpm) + "&c=" + toString(BLEVolt) +  "&d=" + toString(BLECurr) +  "&e=" + toString(BLETemp) + "&f=" + toString(BLEStatus)
                      + "&g=" + toString(10) + "&h=" + toString(BattCellMax) + "&i=" + toString(BattCellMin) + "&j=" + toString(10) + "&k=" + toString(10)
                      + "&l=" + toString(10) + "&m=" + toString(BLEErrorcode) + "&n=" + toString(10) + "&o=" +  toString(ERRORNODEint)
                      + "&p=" + BLE_ErrorList + "&q=" + errorIcon + "&r=" + toString(01) + "&s=" + "DALY" +  "&t=" + toString(10) + "&u=" + toString(10);    // MOTORTEMP;
    // + "&LASTERROR=" + "Battery Low Voltage";  /// battery-half-outline

    pCharacteristic->setValue(s);
    pCharacteristic->notify();

    BLE_ErrorList = ""; /// Reset Error after sent to BLE
    BLEErrorcode = 0;
    ERRORNODEint = 0;

    /// RESET
    for (int i = 0; i < 8; i++)
    {
      data98_2[i] = 0;
    }
  }

  // disconnecting
  if (!deviceConnected && oldDeviceConnected) {
    delay(500); // give the bluetooth stack the chance to get things ready
    pServer->startAdvertising(); // restart advertising
    Serial.println("start advertising");
    oldDeviceConnected = deviceConnected;
  }
  // connecting
  if (deviceConnected && !oldDeviceConnected) {
    // do stuff here on connecting
    oldDeviceConnected = deviceConnected;
  }
}

void onReceive(int packet)
{
  //  Serial.print("packet with id 0x");
  //  Serial.println(CANESP.packetId(), HEX);
  //  dataIds[idscounter] = CANESP.packetId();
  //  idscounter++;

  if (idscounter > 12) idscounter = 0;

  for (int i = 0; i < 8; i++)
  {
    if (CANESP.available())
    {
      counterdatamissing = 0;
      if (CANESP.packetId() == 0x18904001)
      {
        if (CANESP.packetId() == 0x18904001) data90_1[i] = CANESP.read();
        LEDstatusG();
      }
      else if (CANESP.packetId() == 0x18914001)
      {
        data91_1[i] = CANESP.read();
        LEDstatusG();
      }
      else if (CANESP.packetId() == 0x18924001)
      {
        data92_1[i] = CANESP.read();
        LEDstatusG();
      }
      else if (CANESP.packetId() == 0x18934001)
      {
        data93_1[i] = CANESP.read();
        LEDstatusG();
      }
      else if (CANESP.packetId() == 0x18944001)
      {
        data94_1[i] = CANESP.read();
        LEDstatusG();
      }
      else if (CANESP.packetId() == 0x18984001)
      {
        data98_1[i] = CANESP.read();
        LEDstatusG();
      }
      else if (CANESP.packetId() == 0x18904002)
      {
        data90_2[i] = CANESP.read();
        LEDstatusG();
      }
      else if (CANESP.packetId() == 0x18914002)
      {
        data91_2[i] = CANESP.read();
        LEDstatusG();
      }
      else if (CANESP.packetId() == 0x18924002)
      {
        data92_2[i] = CANESP.read();
        LEDstatusG();
      }
      else if (CANESP.packetId() == 0x18934002)
      {
        data93_2[i] = CANESP.read();
        LEDstatusG();
      }
      else if (CANESP.packetId() == 0x18944002)
      {
        data94_2[i] = CANESP.read();
        LEDstatusG();
      }
      else if (CANESP.packetId() == 0x18984002)
      {
        data98_2[i] = CANESP.read();
        LEDstatusG();
      }
      else if (CANESP.packetId() == 0x36E)
      {
        dataMotorrmp[i] = CANESP.read();
      }
      else
      {

      }
    }
    else
    {
      // Serial.println("data Received times Max 20");
      // Serial.print(counterdatamissing);
      counterdatamissing++;
      if (counterdatamissing > 20)
      {
        Serial.println("No data Received");
        digitalWrite(LED_R, ON);
        digitalWrite(LED_G, OFF);
      }
    }
  }
}

void DalyCanbus1(int DataId, int BMSid)
{
  int deafultID = 0x18000040;
  int CANbusExID = deafultID | DataId | BMSid;

  // Serial.print("CANbusID ");
  // Serial.println(CANbusExID , HEX);
  CANESP.beginExtendedPacket(CANbusExID);
  CANESP.write(0);
  CANESP.write(0);
  CANESP.write(0);
  CANESP.write(0);
  CANESP.write(0);
  CANESP.write(0);
  CANESP.write(0);
  CANESP.write(0);
  CANESP.endPacket();
  delay(200);

  /*
    /// Max cell Voltage and Min Cell voltage
    CANESP.beginExtendedPacket(0x18910140);
    CANESP.write(0);
    CANESP.endPacket();
    delay(500);

    /// Max cell Temperature
    CANESP.beginExtendedPacket(0x18920140);
    CANESP.write(0);
    CANESP.endPacket();
    delay(500);

    /// Relay status charge or discharge Relay? Battery
    /// BMS Life ?
    /// Capacity to caluculate the runtime in Min
    CANESP.beginExtendedPacket(0x18930140);
    CANESP.write(0);
    CANESP.endPacket();
    delay(500);

    /// SoH can be caluculated from the charge and discharge cycles ?
    /// Life time charged energy is as number cycles * nominal energy
    CANESP.beginExtendedPacket(0x18940140);
    CANESP.write(0);
    CANESP.endPacket();
    delay(500);
    /// operational Hours are caluculated by ABC

    /// Current Error
    CANESP.beginExtendedPacket(0x18980140);
    CANESP.write(0);
    CANESP.endPacket();
    delay(500);
    /// Last error code ABC will saved
    /// save all the error bytes text data and display in app accordingly
    ///  Run time from abc , from Available capacity / current*/
}

void LEDstatusG()
{
  digitalWrite(LED_G, ON);
  digitalWrite(LED_R, OFF);
}
// ID=305,Type=D,Length=8,Data=EF 01 FF FA DC 00 E8 03,CycleTime=5000,Paused=1,IDFormat=hex

void ErrorlistData(int byteN, long databits)
{
  // Byte 0
  if (byteN == 0)
  {
    if (Binarydata1[0] == 1) BLE_ErrorList = BLE_ErrorList + "OSW over voltage1,"; //Bit 0:
    if (Binarydata1[1] == 1) BLE_ErrorList = BLE_ErrorList + "OSW over voltage2,"; //Bit 1:
    if (Binarydata1[2] == 1) BLE_ErrorList = BLE_ErrorList + "OSW over voltage3,"; //Bit 2:
    if (Binarydata1[3] == 1) BLE_ErrorList = BLE_ErrorList + "TSW over voltage,"; //Bit 3:
    if (Binarydata1[4] == 1) BLE_ErrorList = BLE_ErrorList + "TV too high1,"; //Bit 4:
    if (Binarydata1[5] == 1) BLE_ErrorList = BLE_ErrorList + "TV too high2,"; //Bit 5:
    if (Binarydata1[6] == 1) BLE_ErrorList = BLE_ErrorList + "TV too low1,";  //Bit 6:
    if (Binarydata1[7] == 1) BLE_ErrorList = BLE_ErrorList + "TV too low2,";  //Bit 7:
    BLEErrorcode = databits;
    if (BattCellMin < 3.0) errorIcon = "0";
    if (BattCellMin > 3.8) errorIcon = "1";
  }

  //Byte 1
  if (byteN == 1)
  {
    if (Binarydata1[0] == 1) BLE_ErrorList = BLE_ErrorList + "CT too high1,"; //Bit 0:
    if (Binarydata1[1] == 1) BLE_ErrorList = BLE_ErrorList + "CT too high2,"; //Bit 1:
    if (Binarydata1[2] == 1) BLE_ErrorList = BLE_ErrorList + "CT too low1,";  //Bit 2:
    if (Binarydata1[3] == 1) BLE_ErrorList = BLE_ErrorList + "CT too low2,";  //Bit 3:
    if (Binarydata1[4] == 1) BLE_ErrorList = BLE_ErrorList + "CT too high1,"; //Bit 4:
    if (Binarydata1[5] == 1) BLE_ErrorList = BLE_ErrorList + "CT too high2,"; //Bit 5:
    if (Binarydata1[6] == 1) BLE_ErrorList = BLE_ErrorList + "DT too low1,";  //Bit 6:
    if (Binarydata1[7] == 1) BLE_ErrorList = BLE_ErrorList + "DT too low2,";  //Bit 7:
    BLEErrorcode = databits;
    if (BLETemp < 0) errorIcon = "2";
    if (BLETemp > 0) errorIcon = "3";
  }
  //Byte 2
  if (byteN == 2)
  {
    if (Binarydata1[0] == 1) BLE_ErrorList = BLE_ErrorList + "C over current1,"; //Bit 0:
    if (Binarydata1[1] == 1) BLE_ErrorList = BLE_ErrorList + "C over current2,"; //Bit 1:
    if (Binarydata1[2] == 1) BLE_ErrorList = BLE_ErrorList + "D over current1,"; //Bit 2:
    if (Binarydata1[3] == 1) BLE_ErrorList = BLE_ErrorList + "D over current2,"; //Bit 3:
    if (Binarydata1[4] == 1) BLE_ErrorList = BLE_ErrorList + "SOC High1,";       //Bit 4:
    if (Binarydata1[5] == 1) BLE_ErrorList = BLE_ErrorList + "SOC High2,";       //Bit 5:
    if (Binarydata1[6] == 1) BLE_ErrorList = BLE_ErrorList + "SOC Low1,";        //Bit 6:
    if (Binarydata1[7] == 1) BLE_ErrorList = BLE_ErrorList + "SOC Low2,";        //Bit 7:
    BLEErrorcode = databits;
    if ((Binarydata1[4] == 1) | (Binarydata1[5] == 1) | (Binarydata1[6] == 1) | (Binarydata1[7] == 1))
    {
      if (BattCellMin < 3.0) errorIcon = "0";
      if (BattCellMin > 3.8) errorIcon = "1";
    }
  }
  //Byte 3
  if (byteN == 3)
  {
    if (Binarydata1[0] == 1) BLE_ErrorList = BLE_ErrorList + "ED pressure1,";  //Bit 0:
    if (Binarydata1[1] == 1) BLE_ErrorList = BLE_ErrorList + "ED pressure2,";  //Bit 1:
    if (Binarydata1[2] == 1) BLE_ErrorList = BLE_ErrorList + "ET difference1,";//Bit 2:
    if (Binarydata1[3] == 1) BLE_ErrorList = BLE_ErrorList + "ET difference2,";//Bit 3:
    BLEErrorcode = databits;
  }
  //Byte 4
  if (byteN == 4)
  {
    if (Binarydata1[0] == 1) BLE_ErrorList = BLE_ErrorList + "CMOS Temp,"; //Bit 0:
    if (Binarydata1[1] == 1) BLE_ErrorList = BLE_ErrorList + "DMOS Temp,"; //Bit 1:
    if (Binarydata1[2] == 1) BLE_ErrorList = BLE_ErrorList + "CMOS Temp SF,"; //Bit 2:
    if (Binarydata1[3] == 1) BLE_ErrorList = BLE_ErrorList + "DMOS TEMP SF,"; //Bit 3:
    if (Binarydata1[4] == 1) BLE_ErrorList = BLE_ErrorList + "CMOS AF,"; //Bit 4:
    if (Binarydata1[5] == 1) BLE_ErrorList = BLE_ErrorList + "DMOS AF,"; //Bit 5:
    if (Binarydata1[6] == 1) BLE_ErrorList = BLE_ErrorList + "CMOS BF,"; //Bit 6:
    if (Binarydata1[7] == 1) BLE_ErrorList = BLE_ErrorList + "DMOS BF,"; //Bit 7:
    BLEErrorcode = databits;
    if (BLETemp < 0) errorIcon = "2";
    if (BLETemp > 0) errorIcon = "3";
  }
  //Byte 5
  if (byteN == 5)
  {
    if (Binarydata1[0] == 1) BLE_ErrorList = BLE_ErrorList + "AFE chip,";//Bit 0:
    if (Binarydata1[1] == 1) BLE_ErrorList = BLE_ErrorList + "monomer off,";//Bit 1:
    if (Binarydata1[2] == 1) BLE_ErrorList = BLE_ErrorList + "Temp SF,";//Bit 2:
    if (Binarydata1[3] == 1) BLE_ErrorList = BLE_ErrorList + "EEPROM SF,";//Bit 3:
    if (Binarydata1[4] == 1) BLE_ErrorList = BLE_ErrorList + "RTC clock,";//Bit 4:
    if (Binarydata1[5] == 1) BLE_ErrorList = BLE_ErrorList + "Precharge Failure,";//Bit 5:
    if (Binarydata1[6] == 1) BLE_ErrorList = BLE_ErrorList + "vehicle comm,";//Bit 6:
    if (Binarydata1[7] == 1) BLE_ErrorList = BLE_ErrorList + "intranet comm,";//Bit 7:
    BLEErrorcode = databits;
  }
  //Byte 6：
  if (byteN == 6)
  {
    if (Binarydata1[0] == 1) BLE_ErrorList = BLE_ErrorList + "Current Module,"; //Bit 0:
    if (Binarydata1[1] == 1) BLE_ErrorList = BLE_ErrorList + "pressure module,"; //Bit 1:
    if (Binarydata1[2] == 1) BLE_ErrorList = BLE_ErrorList + "Short circuit,";//Bit 2:
    if (Binarydata1[3] == 1) BLE_ErrorList = BLE_ErrorList + "LowVolt NoChar,";//Bit 3:
    if (Binarydata1[4] == 1) BLE_ErrorList = BLE_ErrorList + "GPS|Soft,";//Bit 4:
    //Bit 5~Bit7: Reserved
    BLEErrorcode = databits;
  }
  // Byte 7
  if (byteN == 7)
  {
    if (Binarydata1[0] == 1) BLE_ErrorList = BLE_ErrorList + "Fault code,"; ///(if 0 x 03, show "fault code 3",0 do not show)
    BLEErrorcode = databits;
  }
}
//Function to convert Binary to Decimal
void convertDecimalToBinary(int databyte, long DecimalV, int Nodeid) {
  int i = 0;
  long Errocode = DecimalV;
  if (ERRORNODEint == 0) ERRORNODEint = Nodeid;
  for (int i1 = 0; i1 < 8; i1++)
  {
    Binarydata[i1] = 0;
  }
  while (DecimalV) {
    //Converts Binary to Decimal
    Binarydata[i] = DecimalV % 2;
    // Serial.print(Binarydata[i]);
    // Serial.print("-");
    DecimalV = DecimalV / 2;
    i++;
  }
  reverse(Binarydata, 8);
  if (Serialprio2) Serial.println(" ");
  ErrorlistData(databyte, Errocode);
}
// Function to reverse elements of an array
void reverse(int arr[], int n)
{
  int aux[n] = {0, 0, 0, 0, 0, 0, 0, 0};
  for (int i = 0; i < n; i++) {
    aux[n - 1 - i] = arr[i];
  }
  for (int i = 0; i < n; i++) {
    Binarydata1[i] = aux[i];
    if (Serialprio2) Serial.print(Binarydata1[i]);
    if (Serialprio2) Serial.print("-");
  }
}
